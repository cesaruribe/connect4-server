import numpy as np
import json
import shortuuid
import crud


def extract_info(mydict, keys):
    return {k: v for k, v in mydict.items() if k in keys}


def set_user_response(info, keys):
    return json.dumps(extract_info(info, keys))


def set_by_checked_userid(userdict):
    return True if userdict.get("id") == 1 else False


def build_game_uuid(users, use_uuid=False):
    if not use_uuid:
        gameid = "".join((usr.get("sid")[:4] for usr in users))
    else:
        gameid = shortuuid.ShortUUID().random(length=8)
    return gameid


async def save_in_database(game_id, moves, winnerid):
    payload = dict(
        GameUUID=game_id,
        Winner=winnerid,
        Movements=json.dumps({'data': moves}),
        TotalMoves=len(moves),
        Tied=False
    )
    response = await crud.post(payload)
    print("db post:", response)
