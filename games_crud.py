import crud
from fastapi import APIRouter, HTTPException

router = APIRouter()


@router.get("/", status_code=200)
async def read_all_games():
    return await crud.get_all()


@router.get("/uuid/{uuid}")
async def read_by_uuid(uuid: str):
    game_returned = await crud.get_by_uuid(uuid)

    if not game_returned:
        raise HTTPException(status_code=404, detail="Game  not found")

    return game_returned


@router.get("/{id}")
async def read_by_id(id: int):
    game_returned = await crud.get_by_id(id)

    if not game_returned:
        raise HTTPException(status_code=404, detail="Game  not found")

    return game_returned
