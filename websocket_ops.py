import socketio  # type: ignore
from socketio.exceptions import ConnectionRefusedError
import json


import initial_state as gb

from init_function import (
    set_player_id,
    delete_player,
    change_turn,
    reset_board,
    reset_turn,
)

from game_function import check_for_winner, make_play
from utils import set_user_response, build_game_uuid, save_in_database


sio = socketio.AsyncServer(
    async_mode="asgi",
    socketio_path="/ws/socket.io",
    cors_allowed_origins=[])
socket_app = socketio.ASGIApp(sio)


users = []
gb.init_board()


@sio.event
async def connect(sid, environ, auth):
    global users
    gb.nboard, gb.cboard = reset_board()

    print("connect ", sid)

    if len(users) < 2:

        users.append(sid)
        idx = set_player_id(gb.players, sid)

        await sio.emit(
            "set_player",
            set_user_response(gb.players[idx], {
                              "name", "color", "colorname", "id"}),
            room=sid,
        )
        await sio.emit("connections", len(users))

        print("connected: ", users)

    else:
        err = {"message": "Filled room. There is already 2 players",
               "data": False,
               "type": 'filled'}
        raise ConnectionRefusedError(json.dumps(err))


@sio.event
async def disconnect(sid):
    global users
    users.remove(sid)
    delete_player(gb.players, sid)
    print("Disconnected client ", sid)
    reset_board()
    await sio.emit("connections", len(users))


@sio.on("play")
async def play(sid, data):

    row = data[0]

    player_loc = next(
        (i for i, d in enumerate(gb.players) if d.get("sid") == sid), None
    )

    filled, column, nboard, cboard = make_play(
        gb.nboard, gb.cboard, data, gb.players[player_loc]
    )

    if filled:
        print("row is filled")
        return

    await sio.emit("board", json.dumps(gb.cboard.tolist()))
    gb.moves.append([int(row), int(column)])
    winner = check_for_winner(gb.nboard, row, column)

    if winner is not None:
        gameuuid = build_game_uuid(gb.players)
        await save_in_database(gameuuid, gb.moves, winner)
        await sio.emit("winner", winner)
        gb.nboard, gb.cboard = reset_board()
        return

    next_player = change_turn(gb.players)
    await sio.emit("turn", next_player)


@sio.on("reset_server")
async def reset_all(sid):
    global users
    gb.nboard, gb.cboard = reset_board()
    gb.players = reset_turn(gb.players)
    gb.moves = []
    await sio.emit("reset_client")
