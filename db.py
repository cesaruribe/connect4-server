from databases import Database
from sqlalchemy import create_engine, MetaData

DATABASE_URL = "sqlite:///./connect4_game.db"

engine = create_engine(DATABASE_URL)
metadata = MetaData()

database = Database(DATABASE_URL)
