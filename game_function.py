import numpy as np
import itertools as it


def len_group(group):
    return sum(1 for _ in group)


def search_winner(array, maxcount):
    idwinner = (
        key
        for key, gr in it.groupby(array)
        if (len_group(gr) > (maxcount - 1) and key > 0)
    )
    winners = list(idwinner)
    is_there_winner = True if len(winners) > 0 else False
    return is_there_winner, winners


def count_by_row_or_column(vector, maxcount=4):
    is_there_winner, winner = search_winner(vector, maxcount)
    return winner[0] if is_there_winner else None


def count_by_main_diagonal(array2d, row, col, maxcount=4):
    main_diag = get_main_diagonal(array2d, row, col)
    if main_diag.size > maxcount - 1:
        is_there_winner, winner = search_winner(main_diag, maxcount)
        return winner[0] if is_there_winner else None
    else:
        return None


def count_by_secondary_diagonal(array2d, row, col, maxcount=4):
    sec_diag = get_secondary_diagonal(array2d, row, col)
    if sec_diag.size > maxcount - 1:
        is_there_winner, winner = search_winner(sec_diag, maxcount)
        return winner[0] if is_there_winner else None
    else:
        return None


def get_main_diagonal(array2d, row, col):
    offset = col - row
    return np.diag(array2d, k=offset)


def get_secondary_diagonal(array2d, row, col):
    offset = (array2d.shape[1] - 1) - row - col
    return np.diag(np.fliplr(array2d), k=offset)[::-1]


def play_by_left(nboard, cboard, row, color, id):
    isFilled, idx = row_is_filled(nboard[row])
    if not isFilled:
        nboard[row, idx[0]] = id
        cboard[row, idx[0]] = color
    return isFilled, idx[0] if idx != [] else idx, nboard, cboard


def play_by_right(nboard, cboard, row, color, idplayer):

    isFilled, idx = row_is_filled(nboard[row])
    if not isFilled:
        nboard[row, idx[-1]] = idplayer
        cboard[row, idx[-1]] = color

    return (isFilled, idx[-1] if idx != [] else idx, nboard, cboard)


def row_is_filled(row):
    idx = np.where(row == 0)[0]
    return False if idx.size > 0 else True, idx


def make_play(nboard, cboard, play, user):

    row, side = play
    if side == "left":
        return play_by_left(nboard, cboard, row, user["color"], user["id"])
    elif side == "right":
        return play_by_right(nboard, cboard, row, user["color"], user["id"])
    else:
        print("A valid side was not given")


def check_for_winner(nboard, row, col):

    winner = count_by_row_or_column(nboard[row])
    if not (winner is None):
        return winner

    winner = count_by_row_or_column(nboard[:, col])
    if not (winner is None):
        return winner

    winner = count_by_main_diagonal(nboard, row, col)
    if not (winner is None):
        return winner

    winner = count_by_secondary_diagonal(nboard, row, col)
    if not (winner is None):
        return winner

    return None
