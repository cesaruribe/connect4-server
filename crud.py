from models import games
from db import database


async def get_all():
    query = games.select()
    return await database.fetch_all(query=query)


async def get_by_uuid(game_uuid):
    query = games.select().where(game_uuid == games.c.GameUUID)
    return await database.fetch_one(query=query)


async def get_by_id(id):
    query = games.select().where(id == games.c.Id)
    return await database.fetch_one(query=query)


async def post(payload):
    query = games.insert().values(payload)
    response = await database.execute(query=query)
    return response
