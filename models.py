from sqlalchemy import Column, DateTime, String, Integer, Table, Boolean
from sqlalchemy.sql import func
from db import metadata

games = Table(
    "games",
    metadata,
    Column("Id", Integer,  primary_key=True, autoincrement=True, index=True),
    Column("GameUUID", String, nullable=False),
    Column("Winner", Integer),
    Column("Tied", Boolean),
    Column("Movements", String),
    Column("TotalMoves", Integer),
    Column("Date", DateTime, default=func.now())
)
