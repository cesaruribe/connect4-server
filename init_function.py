import numpy as np
from initial_state import void
from utils import set_by_checked_userid


def reset_board(sizeX=7, sizeY=7):
    nboard = np.zeros((sizeX, sizeY))
    cboard = np.full((sizeX, sizeY), void)
    return nboard, cboard


def reset_turn(userlist):
    return [{**d, "turn": set_by_checked_userid(d)} for d in userlist]


def set_player_id(play_role, sid):
    for i in range(len(play_role)):
        if not play_role[i]["assigned"]:
            play_role[i]["sid"] = sid
            play_role[i]["assigned"] = True
            return i


def delete_player(play_role, sid):
    for i in range(len(play_role)):
        if sid == play_role[i]["sid"]:
            play_role[i]["sid"] = ""
            play_role[i]["assigned"] = False


def change_turn(play_role):
    for i in range(len(play_role)):
        if play_role[i]["turn"]:
            play_role[i]["turn"] = False
        else:
            play_role[i]["turn"] = True
            next_player = play_role[i]["id"]
    return next_player
