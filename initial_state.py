import numpy as np

players = [
    {
        "name": "Player 1",
        "color": "#73D2DE",
        "colorname": "Sky Blue",
        "assigned": False,
        "sid": "",
        "turn": True,
        "id": 1,
    },  # sky blue crayola
    {
        "name": "Player 2",
        "color": "#FFBC42",
        "colorname": "Yellow",
        "assigned": False,
        "sid": "",
        "turn": False,
        "id": 2,
    },  # maximum yellow  red
]

void = "#fdfffc"
moves = []


def init_board(sizeX=7, sizeY=7):
    global nboard, cboard
    nboard = np.zeros((sizeX, sizeY))
    cboard = np.full((sizeX, sizeY), void)
