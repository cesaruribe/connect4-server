from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from db import engine,  metadata, database
from websocket_ops import socket_app
import games_crud

metadata.create_all(engine)

app = FastAPI()
appdb = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/hello")
async def root():
    return {"message": "hello world"}


app.mount("/ws", socket_app)
app.include_router(games_crud.router, prefix="/games", tags=["games"])
